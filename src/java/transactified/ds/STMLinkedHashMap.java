/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package transactified.ds;

import java.util.Collections;

import clojure.lang.IPersistentMap;
import clojure.lang.PersistentArrayMap;

public class STMLinkedHashMap<K,V> extends AbstractSTMMap<K,V>
{
  @Override
  protected IPersistentMap initialize() {
    return PersistentArrayMap.create(Collections.emptyMap());
  }
}
