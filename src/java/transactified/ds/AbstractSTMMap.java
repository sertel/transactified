/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package transactified.ds;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import clojure.lang.IDeref;
import clojure.lang.IPersistentMap;
import clojure.lang.PersistentHashMap;
import clojure.lang.Ref;

/**
 * The only way to support transactions on a map is to wrap it into a Ref. We provide
 * fine-grained updates that do not conflict with each other on mutually exclusive entries by
 * wrapping also all values into Refs.
 * 
 * @author sertel
 * 
 */
@SuppressWarnings("unchecked")
public abstract class AbstractSTMMap<K, V> implements Map<K, V>, IDeref {
  
  private Ref _map = null;
  
  public AbstractSTMMap() {
    _map = new Ref(initialize());
  }
  
  protected abstract IPersistentMap initialize();
  
  @Override
  public Object deref() {
    return _map.deref();
  } 
    
  @Override
  public int size() {
    return ((PersistentHashMap) _map.deref()).size();
  }
  
  @Override
  public boolean isEmpty() {
    return ((PersistentHashMap) _map.deref()).isEmpty();
  }
  
  @Override
  public boolean containsKey(Object key) {
    // does not work for fine-grained access
    return ((PersistentHashMap) _map.deref()).containsKey(key);
  }
  
  @Override
  public boolean containsValue(Object value) {
    // does not work for fine-grained access
    throw new UnsupportedOperationException();
    // return ((PersistentHashMap) _map.deref()).containsValue(value);
  }
  
  @Override
  public V get(Object key) {
    return (V) ((Ref) ((PersistentHashMap) _map.deref()).valAt(key)).deref();
  }
  
  @Override
  public V put(K key, V value) {
    if(containsKey(key)) {
      // fine-grained entry updates -> only update the entry not the map (allows for concurrent
      // non-conflicting entry updates)
      Ref r = (Ref) ((PersistentHashMap) _map.deref()).valAt(key);
      V old = (V) r.deref();
      r.set(value);
      return old;
    } else {
      // insertion -> map update
      IPersistentMap newMap = ((PersistentHashMap) _map.deref()).assoc(key, new Ref(value));
      _map.set(newMap);
      return null;
    }
  }
  
  @Override
  public V remove(Object key) {
    // deletion -> map update
    Ref old = (Ref) get(key);
    IPersistentMap newMap = ((PersistentHashMap) _map.deref()).without(key);
    _map.set(newMap);
    return (V) old.deref();
  }
  
  @Override
  public void putAll(Map<? extends K, ? extends V> m) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public void clear() {
    _map.set(null);
  }
  
  @Override
  public Set<K> keySet() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public Collection<V> values() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public Set<java.util.Map.Entry<K, V>> entrySet() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
}