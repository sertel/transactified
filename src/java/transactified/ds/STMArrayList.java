/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package transactified.ds;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import clojure.java.api.Clojure;
import clojure.lang.IDeref;
import clojure.lang.IFn;
import clojure.lang.PersistentList;
import clojure.lang.Ref;

public class STMArrayList<E> implements List<E>, IDeref {
  
  private static final String deleteScript =
      "(do (fn [col idx] (reverse (into '() (keep-indexed (fn [index item] (if (= index idx) nil item)) col)))))";
  private IFn _remove =
      (IFn) Clojure.var("clojure.core", "eval").invoke(Clojure.var("clojure.core", "read-string").invoke(deleteScript));
  
  private static final String updateScript =
      "(do (fn [col idx new-item] (reverse (into '() (map-indexed (fn [index item] (if (= index idx) new-item item)) col)))))";
  private IFn _update =
      (IFn) Clojure.var("clojure.core", "eval").invoke(Clojure.var("clojure.core", "read-string").invoke(updateScript));
  
  private Ref _list = new Ref(PersistentList.EMPTY);
  
  @Override
  public Object deref() {
    return _list.deref();
  }
  
  @Override
  public int size() {
    return ((PersistentList) _list.deref()).count();
  }
  
  @Override
  public boolean isEmpty() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public boolean contains(Object o) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public Iterator<E> iterator() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public Object[] toArray() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public <T> T[] toArray(T[] a) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public boolean add(E e) {
    _list.set(((PersistentList) _list.deref()).cons(new Ref(e)));
    return true;
  }
  
  @Override
  public boolean remove(Object o) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public boolean containsAll(Collection<?> c) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public boolean addAll(Collection<? extends E> c) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public boolean addAll(int index, Collection<? extends E> c) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public boolean removeAll(Collection<?> c) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public boolean retainAll(Collection<?> c) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public void clear() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @SuppressWarnings("unchecked")
  @Override
  public E get(int index) {
    return (E) ((Ref) ((PersistentList) _list.deref()).get(index)).deref();
  }
  
  @Override
  public E set(int index, E element) {
    Ref r = (Ref) ((PersistentList) _list.deref()).get(index);
    if(r != null) {
      @SuppressWarnings("unchecked")
      E e = (E) r.deref();
      r.set(element);
      return e;
    } else {
      _list.set(_update.invoke(_list, index, new Ref(element)));
      return null;
    }
  }
  
  @Override
  public void add(int index, E element) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public E remove(int index) {
    E e = get(index);
    _list.set(_remove.invoke(_list, index));
    return e;
  }
  
  @Override
  public int indexOf(Object o) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public int lastIndexOf(Object o) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public ListIterator<E> listIterator() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public ListIterator<E> listIterator(int index) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  @Override
  public List<E> subList(int fromIndex, int toIndex) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }  
}