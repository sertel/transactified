/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package transactified;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import transactified.runtime.STMClassLoading;
import transactified.runtime.STMFunctionExecution;

public abstract class TM {
  
  /**
   * Transforms this class into a version that supports Clojure's concept of software
   * transactional memory and loads the transformed class.
   * 
   * @param cls - the class to be converted
   * @return the converted class as a byte array
   * @throws IOException - problems loading class
   */
  public static Object transformAndLoad(Class<?> cls) throws IOException {
    List<?> o = STMClassLoading.load(Collections.singletonList(cls));
    assert o.size() == 1;
    return o.get(0); 
  }
  
  /**
   * Executes the given callable as a transaction.
   * 
   * @param o - a callable that executes one or more functions against one or more tansactional objects.
   * @throws Exception 
   */
  public static <T> T execute(Callable<T> o) throws Exception {
    return STMFunctionExecution.execute(o);
  }
  
}
