/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package transactified.runtime;

import java.io.IOException;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.janino.ByteArrayClassLoader;

import transactified.compile.STMTransformation;
import transactified.compile.STMTransformation.TransformationImpossible;

public abstract class STMClassLoading {
  
  public static List<?> load(List<Class<?>> ops) throws TransformationImpossible, IOException {
    // need to use a loop here because streams are picky on handling I/O exceptions
    Map<String, byte[]> stmVersions = new HashMap<>();
    for(Class<?> fo : ops) {
      byte[] stmVersion = STMTransformation.transform(fo);
      stmVersions.put(fo.getName() + "STM", stmVersion);
    }
    
    ClassLoader memLoader = classLoad(stmVersions);
    List<Object> objects = new ArrayList<>();
    // now we can essentially use this class loader to get the classes from memory!
    for(Class<?> fo : ops) {
      try {
        Class<?> stmFunction = memLoader.loadClass(fo.getName() + "STM");
        objects.add(stmFunction.newInstance());
        // fo.setFunctionObject(stmFunction.newInstance());
        // fo.setExecution(new STMFunctionExecution());
      }
      catch(ClassNotFoundException | InstantiationException | IllegalAccessException e) {
        assert false : e;
      }
    }
    return objects;
  }
  
  /**
   * We leverage Janino to load the STM versions from memory!
   * 
   * @param classes
   * @return
   */
  public static ClassLoader classLoad(Map<String, byte[]> classes) {
    return AccessController.doPrivileged(new PrivilegedAction<ClassLoader>() {
      @Override
      public ClassLoader run() {
        return new ByteArrayClassLoader(classes, // classes
                                        Thread.currentThread().getContextClassLoader() // parent
        );
      }
    });
    
  }
}
