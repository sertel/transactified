/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package transactified.runtime;

import java.util.concurrent.Callable;

import clojure.lang.LockingTransaction;

public abstract class STMFunctionExecution {
  @SuppressWarnings("unchecked")
  public static <T> T execute(Callable<T> c) throws Exception {
    return (T) LockingTransaction.runInTransaction(c);
  }  
}