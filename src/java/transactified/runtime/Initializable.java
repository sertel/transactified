/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package transactified.runtime;

/**
 * We use this interface to initialize the converted data structures in the class.
 * 
 * @author sertel
 * 
 */
public interface Initializable
{
  public void initializeSTM();
}
