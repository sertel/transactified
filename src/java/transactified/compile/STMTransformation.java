/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package transactified.compile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.util.TraceClassVisitor;

import transactified.ds.STMHashMap;
import transactified.ds.STMLinkedHashMap;
import transactified.runtime.Initializable;
import aua.analysis.AbstractOperatorAnalysis;
import clojure.lang.Ref;

public class STMTransformation extends ClassVisitor {
  private static Map<String, Class<?>> SUPPORTED_COLLECTIONS = new HashMap<>();
  static {
    SUPPORTED_COLLECTIONS.put(Map.class.getName(), STMHashMap.class);
    SUPPORTED_COLLECTIONS.put(HashMap.class.getName(), STMHashMap.class);
    SUPPORTED_COLLECTIONS.put(LinkedHashMap.class.getName(), STMLinkedHashMap.class);
  }
  
  public static class TransformationImpossible extends RuntimeException {
    public String _field = null;
    public String _type = null;
    
    public TransformationImpossible(String field, String type) {
      super("field: " + field + " type: " + type);
      _field = field;
      _type = type;
    }
  }
  
  private class RefInstructionTransformation extends MethodVisitor {
    
    public RefInstructionTransformation(int api, MethodVisitor mv) {
      super(api, mv);
    }
    
    public void visitLocalVariable(String name, String desc, String signature, Label start, Label end, int index) {
      if(name.equals("this")) {
        super.visitLocalVariable(name, "L" + _transformedClass + ";", signature, start, end, index);
      } else {
        super.visitLocalVariable(name, desc, signature, start, end, index);
      }
    }
    
    public void visitFieldInsn(int opcode, String owner, String name, String desc) {
      // TODO scope it to the operator!
      Type t = Type.getType(desc);
      if(AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(t.getClassName())) {
        // we need to generate now the instructions to dereference or set the ref
        switch(opcode) {
          case Opcodes.GETFIELD:
            // GETFIELD <field-reference> : Lclojure/lang/Ref;
            super.visitFieldInsn(opcode, _transformedClass, name, Type.getDescriptor(Ref.class));
            // INVOKEVIRTUAL clojure/lang/Ref.deref ()Ljava/lang/Object;
            String methodDesc = getMethodDescriptor("deref", new Class<?>[0]);
            super.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "clojure/lang/Ref", "deref", methodDesc, false);
            // a cast is needed since Ref only return java.lang.Object
            super.visitTypeInsn(Opcodes.CHECKCAST, _fields.get(name));
            break;
          case Opcodes.GETSTATIC:
            // TODO
            super.visitFieldInsn(opcode, owner, name, desc);
            // Assertion.invariant(false, "unsupported operation");
            break;
          case Opcodes.PUTFIELD:
            // original:
            // PUTFIELD <field-reference> : <value-type-desc>
            // the value to be loaded is located on top of the stack now and the object ref
            // underneath
            
            // swap to get the object reference on top
            super.visitInsn(Opcodes.SWAP);
            
            // GETFIELD <field-reference> : Lclojure/lang/Ref;
            // puts the object ref on top of the stack
            super.visitFieldInsn(Opcodes.GETFIELD, _transformedClass, name, Type.getDescriptor(Ref.class));
            
            // SWAP the two (to fullfil the requirements of the invocation)
            super.visitInsn(Opcodes.SWAP);
            
            // INVOKEVIRTUAL clojure/lang/Ref.set (Ljava/lang/Object;)Ljava/lang/Object;
            methodDesc = getMethodDescriptor("set", new Class<?>[] { Object.class });
            super.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "clojure/lang/Ref", "set", methodDesc, false);
            break;
          case Opcodes.PUTSTATIC:
            // TODO
            throw new UnsupportedOperationException();
          default:
            assert false;
        }
      } else {
        String o = owner.equals(_originalClass) ? _transformedClass : owner;
        super.visitFieldInsn(opcode, o, name, desc);
      }
    }
    
  }
  
  private class ConstructorTransformation extends MethodVisitor {
    public ConstructorTransformation(int api, MethodVisitor mv) {
      super(api, mv);
    }
    
    public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
      assert opcode == Opcodes.INVOKESPECIAL;
      super.visitMethodInsn(opcode, owner, name, desc, itf);
      
      // finish the constructor
      super.visitInsn(Opcodes.RETURN);
      super.visitMaxs(1, 1);
    }
  }
  
  private class CollectionTransformation extends MethodVisitor {
    public CollectionTransformation(int api, MethodVisitor mv) {
      super(api, mv);
    }
    
    public void visitTypeInsn(int opcode, String type) {
      String objectType = Type.getObjectType(type).getClassName();
      String t = null;
      if(opcode == Opcodes.NEW && SUPPORTED_COLLECTIONS.containsKey(objectType)) {
        t = Type.getInternalName(SUPPORTED_COLLECTIONS.get(objectType));
      } else {
        t = type;
      }
      super.visitTypeInsn(opcode, t);
    }
    
    // TODO check the field!
    public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
      String clsName = Type.getObjectType(owner).getClassName();
      String o = null;
      if(opcode == Opcodes.INVOKESPECIAL && SUPPORTED_COLLECTIONS.containsKey(clsName)) {
        o = Type.getInternalName(SUPPORTED_COLLECTIONS.get(clsName));
      } else {
        o = owner;
      }
      super.visitMethodInsn(opcode, o, name, desc, itf);
    }
  }
  
  private class MethodProxy extends MethodVisitor {
    private boolean _done = false;
    private MethodVisitor _initTarget = null;
    
    public MethodProxy(int api,
                       MethodVisitor methodVisitor,
                       MethodVisitor newTarget)
    {
      super(api, methodVisitor);
      mv = new ConstructorTransformation(api, methodVisitor);
      _initTarget = newTarget;
    }
    
    public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
      super.visitMethodInsn(opcode, owner, name, desc, itf);
      if(!_done && Opcodes.INVOKESPECIAL == opcode) {        
        // create a new function for initialization
        createInitialization(_initTarget);
        
        // now, rewrite all the references to the changed fields
        mv = new CollectionTransformation(api, new RefInstructionTransformation(api, _initTarget));
        _done = true;
      }
    }
        
    private void createInitialization(MethodVisitor target) {
      // initialize the references!
      for(String field : _fields.keySet())
        initializeReference(target, _transformedClass, field);
      
      target.visitMaxs(4, 1);
    }
    
    /**
     * Initialization incorporates the following steps/instructions:<br>
     * ALOAD 0<br>
     * NEW clojure/lang/Ref<br>
     * DUP<br>
     * ACONST_NULL<br>
     * INVOKESPECIAL clojure/lang/Ref.<init> (Ljava/lang/Object;)V<br>
     * PUTFIELD <field-reference> : Lclojure/lang/Ref;<br>
     * @param target
     * @param name
     */
    private void initializeReference(MethodVisitor target, String owner, String name) {
      // load 'this'
      target.visitVarInsn(Opcodes.ALOAD, 0);
      // create new Ref object
      target.visitTypeInsn(Opcodes.NEW, "clojure/lang/Ref");
      // duplicate because it is need twice
      target.visitInsn(Opcodes.DUP);
      // load a NULL value
      target.visitInsn(Opcodes.ACONST_NULL);
      // set NULL to constructor of new Ref instance (1. usage)
      String methodDesc = getMethodDescriptor("<init>", new Class<?>[] { Object.class });
      target.visitMethodInsn(Opcodes.INVOKESPECIAL, "clojure/lang/Ref", "<init>", methodDesc, false);
      // store the field to 'this' (2. usage)
      target.visitFieldInsn(Opcodes.PUTFIELD, owner, name, Type.getDescriptor(Ref.class));
    }
  }
  
  private Map<String, String> _fields = new HashMap<>();
  private Map<String, String> _collectionFields = new HashMap<>();
  public String _transformedClass = null;
  public String _originalClass = null;
  
  public STMTransformation(int api, ClassVisitor cv) {
    super(api, cv);
  }
  
  public void visit(int version, int access, String name, String signature, String superName, String[] interfaces)
  {
    _originalClass = name;
    _transformedClass = name + "STM";
    String[] aInterfaces = Arrays.copyOf(interfaces, interfaces.length + 1);
    // TODO What if the operator already implements this interface(s)?! 
    aInterfaces[interfaces.length] = "transactified/runtime/Initializable";
    super.visit(version, access, _transformedClass, signature, superName, aInterfaces);
  }
  
  public void visitInnerClass(String name, String outerName, String innerName, int access) {
    if(name.equals(_originalClass)) super.visitInnerClass(name + "STM", outerName, innerName + "STM", access);
    else super.visitInnerClass(name, outerName, innerName, access);
  }
  
  public void visitOuterClass(String owner, String name, String desc) {
    throw new UnsupportedOperationException("Check if we need to do something here!");
  }
  
  /**
   * Turn Java immutable data types into Clojure refs.
   */
  public FieldVisitor visitField(int access, String name, String desc, String signature, Object value) {
    Type t = Type.getType(desc);
    String refDescriptor = null;
    if(AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(t.getClassName())) {
      refDescriptor = Type.getDescriptor(Ref.class);
      _fields.put(name, t.getInternalName());
    } else if(SUPPORTED_COLLECTIONS.containsKey(t.getClassName())) {
      /**
       * TODO In order to handle reassignments or recreations of lists, we also have to turn
       * these into Refs.
       * <p>
       * However, we currently only handle one-time initializations at object creation (i.e. in
       * the constructor). I will leave this as future work for now.
       */
      // do nothing here. we just have to switch the constructor in the <init>.
      refDescriptor = desc;
      _collectionFields.put(name, t.getDescriptor());
    } else {
      // can't directly throw any other but a runtime exception here.
      throw new TransformationImpossible(name, t.getClassName());
    }
    return super.visitField(access, name, refDescriptor, signature, value);
  }
  
  /**
   * Change access to the "Ref"ed fields.
   */
  public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
    MethodVisitor target = super.visitMethod(access, name, desc, signature, exceptions);
    if(name.equals("<init>")) {
      // we migrate everything out of the constructor!
      // need to create the new method visitor here in order to get a "direct" one.
      String d = getMethodDescriptor(Initializable.class, "initializeSTM", new Class<?>[] {});
      MethodVisitor initializeTarget = super.visitMethod(Opcodes.ACC_PUBLIC, "initializeSTM", d, null, new String[0]);      
      return new MethodProxy(Opcodes.ASM5, target, initializeTarget);
    } else {
      return new RefInstructionTransformation(Opcodes.ASM5, target);
    }
    
  }
  
  private static String getMethodDescriptor(String name, Class<?>[] args) {
    return getMethodDescriptor(Ref.class, name, args);
  }
  
  private static String getMethodDescriptor(Class<?> clz, String name, Class<?>[] args) {
    try {
      if(name.equals("<init>")) return Type.getConstructorDescriptor(clz.getConstructor(args));
      else return Type.getMethodDescriptor(clz.getDeclaredMethod(name, args));
    }
    catch(NoSuchMethodException | SecurityException e) {
      assert false : "It must be that " + clz + " has changed! " + e.getMessage();
    }
    return null;
  }
  
  public static byte[] transform(Class<?> clz) throws IOException {
    return transform(clz, false);
  }
  
  public static byte[] transform(Class<?> clz, boolean print) throws IOException, TransformationImpossible {
    // make sure we load inner classes properly including the host class name
    URL url = clz.getResource(clz.getName().substring(clz.getName().lastIndexOf(".") + 1) + ".class");
    File f = new File(url.getFile());
    assert f.exists();
    
    ClassReader cr = null;
    if(print) {
      System.out.println("ORIGINAL #################################################");
      cr = new ClassReader(new FileInputStream(f));
      cr.accept(new TraceClassVisitor(new PrintWriter(System.out)), ClassReader.SKIP_FRAMES);
    }
    // System.out.println("REWRITE #################################################");
    cr = new ClassReader(new FileInputStream(f));
    ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
    // ClassVisitor logger = new TraceClassVisitor(cw, new PrintWriter(System.out));
    STMTransformation transformer = new STMTransformation(Opcodes.ASM5, cw);
    cr.accept(transformer, ClassReader.SKIP_FRAMES);
    
    return cw.toByteArray();
  }
}