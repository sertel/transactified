# transactified

*transactified* is a small library that enables STM-support for Java classes. It performs a byte code rewrite to wrap the state inside a class into Clojure references and provides functionality to execute it in a Clojure transaction.

## Usage

Let's use the classic account example:
```
#!java
public class Account{
  // state
  private int money = 100;
  
  public void add(int a){
    money += a;
  }
  
  public void withdraw(int w){
    money -= w;
  }
}
```
And here is the transfer from one account to another:

```
#!java
public abstract class Transfer {
  public static void transfer(Account a, Account b, int moneyToTransfer){
    a.withdraw(moneyToTransfer);
    b.add(moneyToTransfer);
  }
}
```
It would be nice to perform the above function atomically. You can do that with *transactified* easily like so:
```
#!java
final Object accountA = transactified.TM.transformAndLoad(Account.class);
final Object accountB = accountA.getClass().newInstance();
transactified.TM.execute(new java.util.concurrent.Callable(){
  public Object call() throws java.lang.Exception {
    Transfer.transfer(accountA, accountB, 50);
    return null;
  }});
``` 

For more advanced conversions of data structures such as hash maps or lists please have a look at our examples ```transactified.ds.STMArrayList``` and ```transactified.ds.STMHashMap```.

## Projects that use this library

[Ohua](https://bitbucket.org/sertel/ohua) - Ohua is an implementation of the stateful functional programming model. It mixes functional and imperative (object-oriented) programming to provide truely transparent parallel execution of programs. It uses this library to transparently convert stateful functions into STM-capable versions and allow for parallel execution of functions that share state.

## Development

Please contact me if you are interested in supporting the development of *transactified*. 

## License

Copyright © 2015 Sebastian Ertel

Distributed under the Eclipse Public License version 1.0 (see the accompanying LICENSE.txt file.)
