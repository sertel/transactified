/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package transactified.runtime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.junit.Test;

import transactified.ds.STMHashMap;
import clojure.lang.IPersistentList;
import clojure.lang.LockingTransaction;
import clojure.lang.PersistentList;
import clojure.lang.Ref;

public class testSTMExecution {
  
  @Test
  public void testNone() {
    // TODO add more testing support
  }
  
  // @Example
  public void testBasicClojureSTMCollections() throws Throwable {
    List<String> initial = new ArrayList<String>();
    initial.addAll(Arrays.asList(new String[] { "bla",
                                               "blub" }));
    final IPersistentList l = PersistentList.create(initial);
    
    /**
     * Clojure transactions work on the basis of immutable data structures. Hence, the
     * surrounding code never sees any update of the transaction.
     * <p>
     * In this example one can say that l is input to the transaction. Hence it can be reused as
     * input over and over again because inside the transaction there will never be a statement
     * that overrides l.
     * <p>
     * However, in Java it would be technically possible to overwrite the reference to l with a
     * new version of it. A Ref object would have to be used in order to handle that case.
     */
    @SuppressWarnings("unused") IPersistentList s =
        (IPersistentList) LockingTransaction.runInTransaction(new Callable<IPersistentList>() {
          @Override
          public IPersistentList call() throws Exception {
            IPersistentList s = (IPersistentList) l.cons("something");
            if(s == null) throw new Exception("some stupid exception");
            else return s;
          }
        });
  }
  
  // @Example
  public void testCollectionRefs() throws Throwable {
    Ref r = new Ref(new STMHashMap<String, String>());
    @SuppressWarnings({ "unchecked",
                       "unused" }) Map<String, String> m = (Map<String, String>) r.deref();
    /**
     * To make the code fully transaction-safe, we have to also make sure that the references to
     * the immutable data structures are Ref's such that we catch renewed assignments.
     */
  }
}
