/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package transactified.compile;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.junit.Assert;
import org.junit.Test;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.util.TraceClassVisitor;

import transactified.compile.STMTransformation.TransformationImpossible;
import transactified.runtime.Initializable;
import transactified.runtime.STMClassLoading;
import clojure.lang.LockingTransaction;

public class testSTMTransformation {
  
  public static class SimpleTestOperator {
    private Integer s = new Integer(322);
    
    public Object[] doSomething() {
      int r = s;
      s = r + 5;
      return new Object[] { "works!" };
    }
  }
  
  public static class CollectionTestOperator {
    private Map<String, String> c = new HashMap<>();
    
    public Object[] doSomething() {
      c.put("key-1", "value-1");
      return new Object[] { c.get("key-1") };
    }
  }

  public static class MemLoadTestOperator {
    private Map<String, String> c = new HashMap<>();
    
    public Object[] doSomething() {
      c.put("key-1", "value-1");
      return new Object[] { c.get("key-1") };
    }
  }

  public static class ImpossibleTestOperator {
    @SuppressWarnings("unused") private ClassReader _testCV = null;
  }
  
  private String transformAndCall(String operator, boolean print) throws Throwable {
    URL url = this.getClass().getResource("testSTMTransformation$" + operator + ".class");
    File f = new File(url.getFile());
    Assert.assertTrue(f.exists());
    
    ClassReader cr = null;
    if(print) {
      System.out.println("ORIGINAL #################################################");
      cr = new ClassReader(new FileInputStream(f));
      cr.accept(new TraceClassVisitor(new PrintWriter(System.out)), ClassReader.SKIP_FRAMES);
    }
    // System.out.println("REWRITE #################################################");
    cr = new ClassReader(new FileInputStream(f));
    ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
    // ClassVisitor logger = new TraceClassVisitor(cw, new PrintWriter(System.out));
    STMTransformation transformer = new STMTransformation(Opcodes.ASM5, cw);
    cr.accept(transformer, ClassReader.SKIP_FRAMES);
    
    ClassLoader loader = STMClassLoading.classLoad(Collections.singletonMap(transformer._transformedClass.replace("/", "."), cw.toByteArray()));

    // check whether the class is loadable and executable!
    @SuppressWarnings("unchecked") final Class<Object> cls =
        (Class<Object>) Class.forName(this.getClass().getName() + "$" + operator + "STM", false, loader);
    final Object op = cls.newInstance();
    Object[] results = (Object[]) LockingTransaction.runInTransaction(new Callable<Object[]>() {
      @Override
      public Object[] call() throws Exception {
        ((Initializable) op).initializeSTM();
        
        // done by FunctionalOperator framework
        Method m = cls.getDeclaredMethod("doSomething", new Class<?>[0]);
        Object[] result = (Object[]) m.invoke(op, new Object[0]);
        return result;
      }
    });
    Assert.assertEquals(1, results.length);
    return (String) results[0];
  }
  
  @Test(timeout = 20000)
  public void testPrimitveType() throws Throwable {
    String result = transformAndCall("SimpleTestOperator", false);
    Assert.assertEquals("works!", result);
  }
  
  @Test(timeout = 20000)
  public void testCollection() throws Throwable {
    String result = transformAndCall("CollectionTestOperator", false);
    Assert.assertEquals("value-1", result);
  }
    
  @Test(timeout = 20000)
  public void testImpossible() throws Throwable {
    try {
      transformAndCall("ImpossibleTestOperator", false);
    }
    catch(TransformationImpossible e) {
      Assert.assertEquals("_testCV", e._field);
      Assert.assertEquals(ClassReader.class.getName(), e._type);
    }
  }
}